#include <stdio.h>
#include <stdlib.h>
#include <xmp.h>

#include <time.h>

#define N 4
#define B 2

void matvecprod(double A[N][N], double x[N], double b[N])
{
#pragma xmp nodes p[B]
#pragma xmp template tm[N][N]
#pragma xmp distribute tm[block][*] onto p
#pragma xmp align A[i][j] with tm[i][j]

#pragma xmp template tv[N]
#pragma xmp distribute tv[block] onto p
#pragma xmp align b[i] with tv[i]

#pragma xmp loop on tm[i][*]
  for (int i = 0; i < N; i++) {
    b[i] = 0.0;
    for (int j = 0; j < N; j++) {
      b[i] += A[i][j] * x[j];
    }
  }
}

int main()
{
#pragma xmp nodes p[B]
#pragma xmp template tm[N][N]
#pragma xmp distribute tm[block][*] onto p
  double A[N][N];
#pragma xmp align A[i][j] with tm[i][j]

  double x[N];

#pragma xmp template tv[N]
#pragma xmp distribute tv[block] onto p
  double b[N];
#pragma xmp align b[i] with tv[i]

  struct drand48_data buffer;
  srand48_r(xmpc_node_num(), &buffer);

#pragma xmp loop on tm[i][j]
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      drand48_r(&buffer, &A[i][j]);
      //printf("A[%d][%d] = %g\n", i, j, A[i][j]);
    }
  }

  // Fill x on one node, broadcast to rest
#pragma xmp task on p[0]
  for (int i = 0; i < N; i++) {
    drand48_r(&buffer, &x[i]);
    //printf("x[%d] = %g\n", i, x[i]);
  }

#pragma xmp bcast (x)

  // Do the mat-vec multiplication. And time it.
  struct timespec tps, tpe;
#pragma xmp barrier
  clock_gettime(CLOCK_MONOTONIC, &tps);
  matvecprod(A,x,b);
#pragma xmp barrier
  clock_gettime(CLOCK_MONOTONIC, &tpe);

  double elapsed_sec = tpe.tv_sec - tps.tv_sec + (tpe.tv_nsec - tps.tv_nsec)
    / 1000000000.0;

#pragma xmp task on p[0]
  printf("Time: %g\n", elapsed_sec);

  return 0;
}
